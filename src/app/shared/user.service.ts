import { Injectable } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  constructor(private fb:FormBuilder, private http:HttpClient) { }
  readonly BaseURI = 'http://localhost:9030/api'

  formModel = this.fb.group({
    firstName:['', [Validators.required]],
    lastName:['', [Validators.required]],
    address:[''],
    age:['', [Validators.required]],
    username:['' ,[Validators.required]],
    password:['', [Validators.required, Validators.minLength(6)]],
    email:['', [Validators.required, Validators.email]],
    accountType:['', [Validators.required]],
    branch:['', [Validators.required]],
    agree:[false, [Validators.requiredTrue]]
  })

  get firstname(){
    return this.formModel.get('firstname');
  }

  get lastName(){
    return this.formModel.get('lastName');
  }
  get address(){
    return this.formModel.get('address');
  }
  get age(){
    return this.formModel.get('age');
  }
  get username(){
    return this.formModel.get('username');
  }
  get password(){
    return this.formModel.get('password');
  }
  get email(){
    return this.formModel.get('email');
  }

  get accountType(){
    return this.formModel.get('accountType');
  }

  get branch(){
    return this.formModel.get('branch');
  }

  get agree(){
    return this.formModel.get('agree');
  }

  register(){
    var body = {
      firstName:this.formModel.value.firstName,
      lastName:this.formModel.value.lastName,
      address:this.formModel.value.address,
      age:this.formModel.value.age,
      username:this.formModel.value.username,
      password:this.formModel.value.password,
      email:this.formModel.value.email,
      accountType:this.formModel.value.accountType,
      branch:this.formModel.value.branch
    };
    console.log(body)
    return this.http.post(this.BaseURI+'/users/register', body);
  }

  login(formData:any){
    console.log(formData);
    return this.http.post(this.BaseURI+'/users/login', formData);
  }
}
